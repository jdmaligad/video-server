import uuid
from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator 


class Room(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=100)
    host = models.ForeignKey(
        'users.CustomUser',
        on_delete=models.CASCADE,
        related_name="hosted_room",
        related_query_name="hosted_rooms",
        blank=True, null=True
    )
    users = models.ManyToManyField('users.CustomUser', blank=True)
    capacity = models.PositiveIntegerField(default=5, validators=[MinValueValidator(5), MaxValueValidator(50)], blank=True, null=True)