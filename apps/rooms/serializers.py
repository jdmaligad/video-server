from rest_framework import serializers
from apps.users.serializers import CustomUserSerializer

from .models import Room


class RoomSerializer(serializers.ModelSerializer):
    user = CustomUserSerializer(required=False)
    users = CustomUserSerializer(many=True, required=False)

    class Meta:
        model = Room
        fields = '__all__'

    def create(self, validated_data):
        user = None
        request = self.context.get('request', None)
        if request and hasattr(request, 'user'):
            user = request.user

        if user:
            validated_data['host'] = user

        created_room = Room.objects.create(**validated_data)
        created_room.users.set([user])

        return created_room


class RoomListSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Room
        fields = '__all__'