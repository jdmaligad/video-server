from django.shortcuts import get_object_or_404
from rest_framework import viewsets
from rest_framework import generics
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response

from .models import Room
from apps.users.models import CustomUser
from .serializers import RoomSerializer, RoomListSerializer


class RoomViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    queryset = Room.objects.all()
    serializer_class = RoomSerializer

    def get_permissions(self):
        """
        Only allows no auth on the get info/detail of a specific room given a guid.
        """
        if self.action == 'retrieve':
            return [AllowAny(), ]
        return super(RoomViewSet, self).get_permissions()

    def get_serializer_class(self):
        """
        Returns limited user detailed room list/detail given a guid (if get room detail)
        as it is stated that there is no auth required, since the user serializer returns
        emails and such. In this case, avoiding returning of sensitive user data.

        Otherwise, uses the fully detail room serializer, given an authentication.
        """
        if self.action == 'list' or self.action == 'retrieve':
            return RoomListSerializer
        return RoomSerializer

    @action(detail=True, methods=['patch'])
    def change_host(self, request, pk=None):
        current_host = request.user
        room = self.get_object()
        new_host = request.data.get('new_host', None)

        if not new_host:
            return Response({"message": "You are not the host of this room."}, status.HTTP_400_BAD_REQUEST)

        if current_host == room.host:
            if room.users.count() > 1:
                new_host = get_object_or_404(CustomUser, pk=new_host)

                if room.users.filter(pk=new_host.pk).exists():
                    room.host = new_host
                    room.save()

                    serializer = self.get_serializer(room)
                    return Response(serializer.data)
            else:
                return Response({"message": "You are the only host participant in this room."}, status.HTTP_400_BAD_REQUEST)
        else:
            return Response({"message": "You are not the host of this room."}, status.HTTP_401_UNAUTHORIZED)

    @action(detail=True, methods=['patch'])
    def join(self, request, pk=None):
        user = request.user
        room = self.get_object()

        if room.host != user and not room.users.filter(pk=user.pk).exists():
            room.users.add(user)
            room.save()

            serializer = self.get_serializer(room)
            return Response(serializer.data)
        else:
            return Response({"message": "You are already in the room."}, status.HTTP_400_BAD_REQUEST)
           
    @action(detail=True, methods=['patch'])
    def leave(self, request, pk=None):
        user = request.user
        room = self.get_object()

        if room.users.filter(pk=user.pk).exists():
            room.users.remove(user)
            if room.host == user:
                if room.users.count():
                    room.host = room.users.first()
                    room.save()
                    serializer = self.get_serializer(room)
                    return Response(serializer.data)
                else:
                    room.delete()
                    return Response({"message": "Room has been deleted due to no more remaining users and host."}, status.HTTP_200_OK)
        else:
            return Response({"message": "You are currently not in the room."}, status.HTTP_400_BAD_REQUEST)


class SearchListView(generics.ListAPIView):
    permission_classes = (AllowAny,)
    serializer_class = RoomListSerializer

    def get_queryset(self):
        """
        This view should return a list of all the rooms in which
        the user is in as determined by the username query parameters.
        """
        queryset = Room.objects.all()
        username = self.request.query_params.get('username', None)
        if username is not None:
            queryset = queryset.filter(users__username=username)
        return queryset