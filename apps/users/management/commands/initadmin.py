from django.core.management.base import BaseCommand
from apps.users.models import CustomUser

class Command(BaseCommand):
    def handle(self, *args, **options):
        if CustomUser.objects.count() == 0:
            username = "admin"
            email = "admin@example.com"
            password = 'admin'
            print('Initializing admin account ...')
            admin = CustomUser.objects.create_superuser(email=email, username=username, password=password)
            admin.is_active = True
            admin.is_admin = True
            admin.save()
            print(f'Admin account initialized with credentials: U: "admin" | P: "admin"')
        else:
            print('Admin accounts can only be initialized if no users exist.')