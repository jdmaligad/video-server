import uuid
from django.contrib.auth.models import AbstractUser
from django.conf import settings
from django.db import models

from apps.rooms.models import Room


class CustomUser(AbstractUser):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    username = models.CharField(unique=True, max_length=100)
    email = models.EmailField(unique=True)
    mobile_token = models.CharField(max_length=255)

    def __str__(self):
        return f"{[self.username]} {self.last_name}, {self.first_name}"
    
    def __unicode__(self):
        return f"{[self.username]} {self.last_name}, {self.first_name}"