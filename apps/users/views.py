from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated, AllowAny

from .models import CustomUser
from .serializers import CustomUserSerializer


class CustomUserViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    queryset = CustomUser.objects.all()
    serializer_class = CustomUserSerializer

    def get_permissions(self):
        """
        Only allows no auth on the get info/detail of a specific room given a guid, list or create.
        """
        if self.action == 'list' or self.action == 'retrieve' or self.action == 'create':
            return [AllowAny(), ]
        return super(CustomUserViewSet, self).get_permissions()